from django.urls import path
from todos.views import (
    todo_list, todo_detail,
    todo_create, todo_update,
    todo_delete, item_create,
    item_update)


urlpatterns = [
    path("todos/", todo_list, name="todo_list_list"),
    path("todos/<int:id>/", todo_detail, name="todo_list_detail"),
    path("todos/create/", todo_create, name="todo_list_create"),
    path("todos/<int:id>/edit/", todo_update, name="todo_list_update"),
    path("todos/<int:id>/delete/", todo_delete, name="todo_list_delete"),
    path("todos/items/create/", item_create, name="todo_item_create"),
    path("todos/items/<int:id>/edit/", item_update, name="todo_item_update"),
]
