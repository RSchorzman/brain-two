# Generated by Django 4.2.6 on 2023-10-31 21:24

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0003_alter_todoitem_options"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
