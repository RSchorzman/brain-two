# Generated by Django 4.2.6 on 2023-10-31 22:49

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0004_alter_todoitem_due_date"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="todoitem",
            options={},
        ),
    ]
