from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, UtodoForm, ItemForm, UitemForm

# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/lists.html", context)


def todo_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": list,
    }
    return render(request, "todos/details.html", context)


def todo_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = UtodoForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = UtodoForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/icreate.html", context)


def item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = UitemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = UitemForm(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todos/iupdate.html", context)
