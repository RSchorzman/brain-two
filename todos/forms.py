from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class UtodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
        # utodo = TodoList.objects.get(pk=1)
        # form = TodoForm(instance=utodo)
        # edit_only = True


class ItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]


class UitemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
        # uitem = TodoItem.objects.get(pk=1)
        # form = ItemForm(instance=uitem)
        # edit_only = True
